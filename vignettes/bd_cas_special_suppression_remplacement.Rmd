---
title: "bd_cas_special_suppression_remplacement"
author: "LBn"
date: "2022-12-15"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{bd_cas_special_suppression_remplacement}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```




## Suppression/remplacement de données 

Notamment en vu d'un remplacement après réception d'un correctif, 
dans l'exemple donné en suivant, les enregistrements 
à remplacer concernent le mois d'avril 2021.


```{r eval=FALSE}
library(rsvero.immatriculations)
rsvero2 <- bd_lire(chemin_vers_bdd) # prend du temps

# etape 1. suppression des données objets du remplacement
mois_a_supprimer <- c(2021,4)

# ctrl : bd_statut(rsvero2) #/# bd_recenser_millesimes(rsvero2)
rsvero2$donnees <- rsvero2$donnees %>% 
  filter(!(annee==mois_a_supprimer[1] & mois==mois_a_supprimer[2])) 
# ctrl : bd_statut(rsvero2) #/# bd_recenser_millesimes(rsvero2)  

# etape 2. alimentation normale de la base de données
bd_alimentation_mensuelle(rsvero2, 2021, 4, d_updateFilesDir)
# ctrl : bd_statut(rsvero2) #/# bd_recenser_millesimes(rsvero2)

# final : sauvegarde de la correction
bd_sauver(rsvero2)
```




