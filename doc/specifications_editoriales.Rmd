---
title: "specifications_editoriales"
output: html_document
---



# Structure du document au 1er janvier 2022

## entête de d'identitification sur service émetteur

## titre + encart de présentation du document

## titre 1 : répartition des immatriculations de voitures neuves en Juin 2021, avec zoom sur les motorisations alternatives

  - fig.1 : camembert double, parts en pourcentage des différentes catégories de véhicule en termes de motorisation avec       zoom particulier sur les catégories dites alternatives
  
## titre 1 : Immatriculations cumulées sur douze mois des voitures neuves depuis 2016

  - fig.2 : "

  - fig.3 : courbe double selon l'utilisateur (personne morale/personne physique)
  
  - fig.4 : chroniques selon le type de motorisation
  
  - fig.5 : chroniques selon le type de motorisation selon l'utilisateur

  - fig.6 : zoom sur les voitures neuves à motorisation alternative

## titre 1 : Emissions moyens de CO2 des voitures neuves depuis 2016

  - fig.7 : chronique des émissions moyennes (courbe présentant la fameuse rupture méthodologique)


## titre 1 : Immatriculations dans les DOM (hors Mayotte) en Juin 2021

  - fig.8 : histogramme par département selon les types de motorisations (note : bleu hybride)

  - fig.9 : tableau effectifs autres types de véhicules neufs par département et par genre
  
## glossaire / liste de définitions

## encart de liens

  - page de référence de la Deal
  - lien avec la publication nationale
  
## encart "crédits"
  
========================================================================================  
  

# Structure du document objectivé par le groupe

## entête de d'identitification sur service émetteur

## titre + encart de présentation du document


!! NOUVEAUTE : encart avec texte conjoncturel à saisir !!

## titre 1 : répartition des immatriculations de voitures neuves en Juin 2021, avec zoom sur les motorisations alternatives

  - fig.1 : camembert double, parts en pourcentage des différentes 
    catégories de véhicule en termes de motorisation avec       
    zoom particulier sur les catégories dites alternatives
  
    inspiration possible ![exemple statinfo véhicules](./illustrations/ce_20220429_0934.png)
    
    
    
## titre 1 : Immatriculations cumulées sur douze mois des voitures neuves depuis 2016

  - fig.2 : "

  - fig.3 : courbe double selon l'utilisateur (personne morale/personne physique)
  
  - fig.4 : chroniques selon le type de motorisation
  
  - fig.5 : chroniques selon le type de motorisation selon l'utilisateur

  - fig.6 : zoom sur les voitures neuves à motorisation alternative

---  ## titre 1 : Emissions moyens de CO2 des voitures neuves depuis 2016
---  
---    - fig.7 : chronique des émissions moyennes (courbe présentant la fameuse rupture 
---      méthodologique)
---  

## titre 1 : Immatriculations dans les DOM (hors Mayotte) en Juin 2021

  - fig.8 : histogramme par département selon les types de motorisations (note : bleu hybride)

  - fig.9 : tableau effectifs autres types de véhicules neufs par département et par genre
  
## glossaire / liste de définitions

## encart de liens

  - page de référence de la Deal
  - lien avec la publication nationale
  
## encart "crédits  