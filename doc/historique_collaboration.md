<!--
Date MàJ : 2022-06-17
-->

# 2022-06-29\|visio

Connectés : jfCn, pMy, rMe, lBn

  - On étudie la mise à jour de la base avec les fichiers mensuels.
  Il faut aussi penser aux correctifs probable provenant du ministère (exemple d'avril 2021 corrigé).
  
  - La structure du fichier des immats d'occasions est identique au fichier du neuf.
  Donc possibilité de les intégrer dans la publication.
  - Laurent nous explique comment fonctionne le "connecteur_rsvero2.R"
  
  Création de la fonction read_stat_x_dreal_dep.R dans la branche FNalim
  
  Création d'une branche Doc-dev par lBn
  
  Création d'un répertoire sur la racine rsvero.immatriculations "vignette"
  et initialisation du mode d'emploi de la mise à jour de la base locale.
  
  
  

# TODO

  - comment maintenir la base de données locale : rédaction du mode d'emploi
  - réflexion sur une mise en page (jfCn)
  - graphe camembert :en partant de la généralisation de la fonction `grph_repartition_selon_motorisation`

# 2022-06-22\|Visio

Connectés : jfCn, pMy, rMe, lBn

  - debugage de l'utilisation des fichiers dans le répertoire "scripts" qui nécessite
    le placement des données dans le sous-répertoire data (les données ne sont plus dans le repo)
  - travail sur le passage de l'état de maquette proposée par jfCn au format Rmarkdown  
    - introduction en conséquence du concept de chronique et d'une fonction 
      plus générale `grph_courbe` (à partir de `grph_vp_depuis_2016`)et d'une fonction 
      utilitaire `passage_chronique_cumul12mois`

# 2022-06-17\|Visio

Connectés : jfCn, Rme, lBn

séance où l'on 

  - a validé la branche de développement sous la forme d'un embryon de 
    package pour devenir le corps de code officiel (branche main)
  - a espionné les production de la Dreal Hauts-de-France et son organisation,
    - https://www.hauts-de-france.developpement-durable.gouv.fr/IMG/html/publication_interactive_mai_immat_avril.html
    - https://www.hauts-de-france.developpement-durable.gouv.fr/?Mise-en-ligne-du-bulletin-mensuel-sur-les-immatriculations-de-vehicules-neufs
    - http://intra.dreal-hauts-de-france.e2.rie.gouv.fr/emissions-des-vehicules-a13010.html
  - a évoqué, éclairés par les réflexions de jfCn, ce que pourrait être notre projet de tableau de bord, 
    notamment en se focalisant sur VP, VUL, CATL (cf. champ `categ` de rsvero2)
  

# 2022-06-02\|Visio

Connectés : jfCn, pMy, rMe, lBn

Séance où l'on a connecté le modèle de rapport originel à rsvero2 au lieu de l'appuyer encore sur la 
base reconstituée `immat_CO2_dom.rds` à partir des fichiers mensuels RSVéRo envoyés par le BSRV jusque
fin décembre 2021.


# 2022-05-04\|Visio

Aujourd'hui on parle encore de git et gitlab : comment prendre des notes, basculer de 
branche et créer de nouveau éléments pour enrichir la documentation.

Et on parle aussi de point plus précis :

  - Démo de la nouvelle version façon package
  - Analyse et essai comparatif pour comprendre le couac du mois d'avril
  - discussion sur la problématique de constitution d'une base de données 
    locale propre à chacun (idée de la mutualisation)
  

TODO :

  - note de présentation (Philippe)
  - imaginer 4 questions pour que l'interview virtuelle ait un sens (Rodrigue)
  - commiter la nouvelle version (Laurent)
  - voir comment ponter cette nouvelle version avec le nouveau format de base de données (Laurent)
  - pousser la modification relative à la personnalisation du document (Laurent)
  

# 2022-04-29\|Visio

Décision en prévision du changement de poste de Philippe de faire une
opération de communication pour non seulement rappeler le projet lancé par la 
Deal Guadeloupe, mais aussi l'élargissement de ce projet à une communauté 
plus large.

Le matériel prévu pour cet évènement est :
  - [note historique pour présenter le projet](./note_presentation.Rmd), sa genèse et ses objectifs
  - annonce de reprise du `service` (justifiant l'opération de communication)
  - note d'organisation du groupe projet élargi 
  - *document spécifiant* la [ligne éditoriale](./specifications_editoriales.Rmd) et la structure du la publication régulière
  - plus le document de communication propre à chaque entité
  - ? idée d'une vidéo de présentation de l'effort partage pour arrivé à cette nouvelle forme de publication
  
  
  
  

# 2022-04-14\|Visio

Les sujets traités furent :
  - explorer l'interface web Gitlab (vaste question de comment retrouver un commit)
    - pour évoquer la création d'une banche à partir d'un commit spécifique
  - mise du pied à l'étrier pour essayer de mettre en application le travail dirigé 
    proposé par LBn dans son mail du 23 mars (création de fonctions)
  - traitement spécifique d'une partie du code au sujet de la méthode de calcul proposé 
    par Philippe MOUTY qui apporte quelques évolutions dans les résultats par rapport 
    au mode de calcul précédent mis au point par Lauranne dans un premier temps.

# 2022-04-08\|Visio

Séance détournée où l'on a finalement parlé de sitadel en travaillant sur une base de code 
fondée par Caroline Coudrin.

# 2022-03-23\|Visio

Où l'on envisage de "masquer" dans des fonctions les blocs de code complexes 
qui peuplent le corps du document

# 2022-03-15\|Visio

Nouveau détournement / digression vers le sujet connexe de la publication de la
Martinique.

# 2022-02-24\|Visio

Où l\'on a exploré les nouvelles livraisons du Sdes et leur incidence
sur l\'infrastructure de production actuelle.

# 2022-02-17\|Visio

Séance de travail pour découvrir la méthode semblable qu'avait suivi la Martinique pour 
sa propre [publication annuelle](https://www.martinique.developpement-durable.gouv.fr/les-immatriculations-de-vehicules-neufs-annee-2020-a1680.html).

# 2022-02-10\|Visio Immatriculations DOM

Paramétrisation à plusieurs mains (voir les logs du gitlab)

# 2022-01-28\|Publication automobile Antilles-Guyane

Du bla bla et comment on s\'organise pour la suite, demo sur la
paramétrisation et un peu théorie sur Git

# 2022-01-13\|Groupe de travail Antilles Guyane : Immatriculations véhicules neufs

La publi en live par Ph. Mouty

# 2021-12-15\|Visio

Visio rapide où l\'on évoque la stratégie et la suite après Lauranne

# 2021-12-10\|Mail

Départ de Lauranne : c\'est mon dernier jour

# 2021-12-06\|Mail

Repo sur Gitlab désormais accessible à tous

# 2021-11-15\|Visio

Retour d\'appropriation, notamment démo que Rodrigue avait pu faire
tourner

# 2021-10-21\|Visio

Visio laborieuse cause problème d\'installation et démo de Lauranne (pas
concluante) trop de problème d\'installation. Elle diffusera un tuto peu
après

# 2021-10-18\|Mail

Rattrapage de la Guyane (Jean-François) dans le groupe.

# 2021-10-07\|Visio

Première visio : présentation du projet et de la démarche, mais gros
problèmes de connexion, visio difficile à suivre.

# 2021-09-29\|Mail

Formulation de la proposition de collaboration

# 2021-09-27\|Série échanges mail

Sur les principes de la confrontation et l\'idée de la mutualisation.

# 2021-09-07\|Mail

Communication générale par Philippe sur la dernière production de la
Guadeloupe.
