---
title: "Question Présentation Vidéo"
author: "RMe"
date: "06/05/2022"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Propositions de questions concernant la présentation du projet.




- Dans quel environnement ou contexte s'inscrit le projet?

- A quels besoins doit répondre le projet?

- Quels sont les enjeux et/ou objectifs du projet?

- Quelles sont les personnes, organisations ou équipes impliquées dans le projet?

- Quels sont les rôles et responsabilités de chacun?

- De quelles ressources dispose t-on pour mener à bien ce projet?

- Quelles sont les grandes étapes du projet?

- Quel plan de communication va t-il être mis en place?

- Quels sont les équipements ou produits utiles à la réalisation du projet?

- Qui est la cible du projet?

- Quel est le planning envisagé?






