---
title: Modop maintenance BDD locale
author: "Laurent Beltran"
date: "29 juin 2022"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Modop maintenance BDD locale}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
options(rmarkdown.html_vignette.check_title = FALSE)
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```



Le présent package `rsvero.immatriculations` regroupe au travers d'une bibliothèque 
quelques fonctions visant à faciliter un peu l'exploitation des données 
fournies mensuellement par le BSRV traitant du sujet des immatriculations.

Notamment, ce paquet offre des fonctions pour maintenir une base de données locale,
reposant sur la livraison initiale réalisée par le BRSV au travers de son envoi
melanissimo du 25 février 2022 et maintenue à jour par alimentation régulière 
au rythme des données mensuelles toujours envoyées par le même BSRV.

Dans un premier temps, nous allons voir comment alimenter la base tous les 
mois grâce aux mises à jour reçue par voie de mail.

Nous reviendrons dans un second temps sur la méthode d'initialisation de
la base de données.

Enfin, nous verrons, sur la base d'un cas réel rencontré en xxxx,
comment apporter un correctif à la base de données.


# Présentation des deux bases de données locales

Avant tout il est envisagé par ce package la volontée de maintenir localement
une base de données. En lieu de base de données, il s'agit en fait 
tout simplement de deux tableaux identiques dans leur structuration,
sauf que l'un est dédié au suivi des immatriculations neuves et l'autre 
au changement de propriétaire dans le cadre du marché d'occasion.

Ces tableaux sont simplement des tibbles sauvegardés au format `rds`, 
nous faisons le choix d'appeler celui relatif aux immatriculations neuves,
`rsvero2-n.rds`, quant à celui relatif aux occasions nous le sauvegarderons 
dans un fichier nommé `rsvero2-o.rds`.

# Amorce de code systématique (à la manière tidyverse)


```{r setup}
library('tidyverse')               ## style adopté
pkgload::load_all('..')            ## tant que le package est en mode développement
library('rsvero.immatriculations') ## en mode package
```

Il est proposé de définir les 2 variables suivantes :

  - `d_updateFilesDir` : répertoire où sont et seront stockés tous les 
     fichiers suivant la convention de nommage  `stats_<x>_dreal_dep_<YYYY>_<MM>.xlsx`
     où `<x>` peut valoir `n` ou `o` respectivement pour des données d'immatriculation       neuve et pour des données d'occasion, où `<YYYY>` représente l'année 
     concernée définie sur quatre chiffres, quand à `<MM>` le mois sur deux chiffres,
     définissant ainsi le millesime de la mise à jour ;
  - `d_dbDir` : répertoire où seront maintenues
     les bases de données : `rsvero2-n.rds`et `rsvero2-o.rds`.

Ces variables correspondent à des chemins complets vers 
des espaces de stockages numériques réels (répertoires informatiques) 
et d'y faire référence dans toutes les commandes présentées en suivant 
par ces alias. 

Il faut donc impérativement rajouter en début les définitions, 
elles dépendent bien évidemment de l'environnement et de l'organisation
de chacun les valeurs données ici ne le sont qu'à titre d'exemple.

```{r}
# répertoire réceptacle des fichiers de mise à jour envoyé par le BSRV
d_updateFilesDir <- r'(S:\8-scpdt\-AccRest-Stat\donnees\sdes\rsvero2\mensuels)'

# lieu de stockage de la base de données locales et nom des tables
d_dbDir <- r'(S:\8-scpdt\-AccRest-Stat\donnees\uoes)'
dbn <- 'rsvero-n.rds'
pthto_dbn <- file.path(d_dbDir,dbn)
dbo <- 'rsvero-o.rds'
pthto_dbo <- file.path(d_dbDir,dbo)
```



# Mise à jour mensuelle 

Opération à faire chaque mois lorsque l'on reçoit une nouvelle livraison du BSRV.


Normalement la base de données rsvero2 a déjà été initialisée lors 
de la livraison faite par le Sdes.



```{r eval=FALSE}
rsvero2_n <- readRDS(pathto_dbn) ## ouverture


maj_fname <- 'stat_n_dreal_dep_2022_05.xlsx'
maj_fpth <- file.path(d_updateFilesDir, maj_fname)

#rsvero2_n %>% distinct(annee, mois) %>% arrange(desc(annee), desc(mois))
rsvero2_n <- alimente_rsvero2(rsvero2_n, c(maj_fpth))
#_controle_# rsvero2_n %>% distinct(annee, mois) %>% arrange(desc(annee), desc(mois))


saveRDS(rsvero2_n, file=pthto_dbn) ## fermeture
```



# Intialisation de la base de données 

## Création de de la base de données à partir d'un fichier initial 

```{r eval=FALSE}
livraison_initiale <- 'stat_n_dreal_dep_2022_01.xlsx'
pthto_li <- file.path(d_updateFilesDir, livraison_initiale)
rsvero2_n <- reshape_resvero2_initiale(pthto_li)

saveRDS(rsvero2_n, file=pthto_dbn)
```


## au besoin suivi de l'insertion d'un lot de rattrapage

```{r eval=FALSE}

rsvero2_n <- readRDS(pathto_dbn)


# Selection des fichiers devant entrer dans la mise à jour
flist <- list.files(d_updateFilesDir, full.names = TRUE) 
flistn <- flist[grepl('stat_n_dreal_dep_', flist)]
tfiles <- flistn[                              # exclusions :   
  !(grepl('stat_n_dreal_dep_2022_01', flistn)) # c'est déjà le fichier d'initialisation
  &
  !(grepl('stat_n_dreal_dep_2021_04', flistn)) # sera traité plus bas dans le doc, cas correctif
  ]

# alimentation effective
rsvero2_n <- alimente_rsvero2(rsvero2_n, tfiles)


saveRDS(rsvero2_n, file=pthto_dbn)
```


# Suppression de données 

Notamment en vu d'un remplacement après réception d'un correctif, les objets 
à remplacer concernent le mois d'avril 2021.



```{r eval=FALSE}
rsvero2_n <- readRDS(pathto_dbn)

# etape 1. suppression des données objets du remplacement
mois_a_supprimer <- c(2021,4)

rsvero2_n_sansAvr2021 <- rsvero2_n %>% 
  filter(!(annee==mois_a_supprimer[1] & mois==mois_a_supprimer[2])) 
  

  # contrôle 
  # rsvero2_n_sansAvr2021 %>%
  #   distinct(annee, mois) %>%
  #   arrange(desc(annee), desc(mois)) %>%
  #   View()



# etape 2. alimentation normale de la base de données
maj_fname <- 'stat_n_dreal_dep_2021_04.xlsx'
maj_fpth <- file.path(d_updateFilesDir, maj_fname)
rsvero2_n <- alimente_rsvero2(rsvero2_n_sansAvr2021,c(maj_fpth))


saveRDS(rsvero2_n, file=pthto_dbn)  
```



