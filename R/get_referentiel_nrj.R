
namedColor <- c(
  'c.DIE' =  '#2E4053',
  'c.ESS' =  '#1976D2',
  'c.VRT' =  '#64DD17',
  'ENR'   =  '#2196F3',
  'ESS'   =  '#5499C7',
  'DNR'   =  '#B0BEC5',
  'DIE'   =  '#78909C',
  'ELH'   =  '#76FF03',
  'HRE'   =  '#7CB342'
)

orderedNrjDet <- c('ELH', 'HRE', 'GAZ', 'DNR', 'DIE', 'ENR', 'ESS', 'AUT')

get_referentiel_nrj <- function()
{
  # définition objets locaux ------------------------------------------------

  ener_code <- clsrgpmt <- NULL  
    
  table_correspondance <- tibble::tibble(
    ener_code = c(
      "ELH",
      "ESS",
      "DIE",
      NA,
      "DNR",
      "ENR",
      "HRE",
      "GAZ",
      "AUT"
    ),
    energie = c(
      "Electrique et hydrog\u00E8ne",
      "Essence thermique",
      "Diesel thermique",
      NA,
      "Diesel - hybride NR",
      "Essence - hybride NR",
      "Hybride rechargeable",
      "Gaz",
      "Autres"
    ),
    clsrgpmt = c(
      'c.VRT',
      'c.ESS',
      'c.DIE',
      NA,
      'c.DIE',
      'c.ESS',
      'c.VRT',
      'c.VRT',
      'c.autre'
    ))

    table_correspondance %>%
    dplyr::mutate(
      det = forcats::fct(ener_code, levels=orderedNrjDet)
    ) %>%
    dplyr::arrange(det) %>%
    dplyr::mutate(
      cls = forcats::fct_inorder(clsrgpmt),
    )
}
