#' grph_line_req02_catl_categ
#'
#' @encoding UTF-8
#'
#' @param .data données telles que préparées par [req02_genre_eu()]
#'
#' @return graphe interactif plotly
#'
#' @include as_millesime.R
#'
#' @export
#'
viz_line_req02_catl_categ <- function(.data)
{

  mytab <- .data
    
  # annee_max <- mytab %>% dplyr::pull(millesime) %>%
  #   lubridate::year() %>% max()

  prct_eff_mn <- c_nudge_x <- c_nudge_y <- cls <- eff <- pos <-
      etiquette <- genre_eu <- millesime <- cumul12m <- NULL
    
  mygraph <- mytab %>%
    dplyr::filter( genre_eu != 'ND') %>%
    ggplot2::ggplot()+
    ggplot2::geom_point(ggplot2::aes(x = millesime, y = cumul12m, color = genre_eu)) +
    ggplot2::geom_line(ggplot2::aes(x = millesime, y = cumul12m, color = genre_eu)) +
    ggplot2::labs(title = "", subtitle = "", x = "", y = "", caption = "") +
    ggplot2::theme_classic()+
    ggplot2::theme(
      axis.text          = ggplot2::element_text(size = 8),
      legend.position    = 'right',
      legend.title       = ggplot2::element_blank(),
      line               = ggplot2::element_line(colour = "grey"),
      panel.background   = ggplot2::element_blank(),
      panel.border       = ggplot2::element_blank(),
      plot.background    = ggplot2::element_blank(),
      text               = ggplot2::element_text(colour = "black", size = 12)
    )
  # +
  #   xlim(lubridate::ymd(20170101),lubridate::ymd(annee_max*100+101))


  plotly::ggplotly(
      mygraph,
      tooltip = c("millesime", "cumul12m")
    ) %>%
    plotly::layout(
      legend     = list(title=list(text='<b>Genre</b>')),
      hoverlabel = list(bgcolor = "white", bordercolor = "grey"),
      yaxis      = list(title = "Nombre \nd'immatriculations"))


}

