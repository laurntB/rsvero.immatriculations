---
title: 'TdB-ivn'
output:
  flexdashboard::flex_dashboard:
    orientation: rows
params:
  cogreg: '03'
  annee: 2022
  mois: 10
---



```{r prologue, echo=FALSE}
library('tidyverse')
library('knitr')
pkgload::load_all('..')
source('configuration_locale.R')
options(knitr.kable.NA = '')
```

```{r preparation configuration locale}
regions <- rsvero2Light$neuf %>% 
  filter(REG == params$cogreg) %>% 
  distinct(REGLIB) %>% 
  pull(REGLIB)
region <-  regions[1]

datemoisp <- lubridate::ym(params$annee*100+params$mois-1)
datemois <- lubridate::ym(params$annee*100+params$mois)
dateaffichemp <- strftime(datemoisp, format="%B %Y")
dateaffiche <- strftime(datemois, format="%B %Y")
```


# Véhicules Très Légers

Row 
-------------------------------------



### Chroniques  pour la région `r region`, chiffres à fin `r dateaffiche`, concernant les différents genres de véhicules composant la catégorie des véhicules très légers

```{r}
grph_catl_chroniques_sous_categ(rsvero2Light$neuf, params$cogreg)
```

Row 
-------------------------------------

### A propos de la transition énergétique et des modes de motorisation vers lesquels se tournent les acheteurs

```{r}
tbl_type_motorisation_avt_cmbrt_mouty(
  rsvero2Light$neuf, 
  params$cogreg, 
  params$annee, params$mois, 
  'CATL') %>% 
  mutate(
    evo_pt = glue::glue("{evo}pt")
  ) %>% 
  select(genre_eu, energie, eff_mnm1, eff_mn, evo_pt) %>% 
  kable(
    format = 'pipe',
    col.names = c(
      'Catégorie de véhicules légers', 
      'Type de motorisation',
      stringr::str_to_title(glue::glue('{dateaffichemp}')),  ## Immatriculations au mois  
      stringr::str_to_title(glue::glue("{dateaffiche}")),    ## Immatriculation au mois 
      "Evolution proportion"),                               ##  de part d'immatriculation 
      align = 'llrrr'
  )
```


### Evolution au mois de `r dateaffiche`

```{r}
grph_cmbrt_mouty_nrj(
  rsvero2Light$neuf, 
  params$cogreg, 
  params$annee, params$mois, 
  'CATL'
)
```

> évolution en rapport à la part globale pour les véhicules très légers 
> en matière d'investissement sur les nouvelles motorisations
> par rapport au mois précédent


# Véhicules Particuliers (voitures au sens large)

### Transition énergétique
```{r}
grph_cmbrt_mouty_nrj(
  rsvero2Light$neuf, 
  params$cogreg, 
  params$annee, params$mois, 
  'VP'
)
```

> Proportion des acquisitions du mois par type de motorisation
> avec en annotation leur évolution relative par rapport au mois précédent

# Crédits


Groupe des hackeurs fous 







