---
title: "prj_tableau_de_bord"
author: "GT"
date: '2022-06-17'
output: html_document
params:
  annee: 2022
  mois: 9
  cogreg: '03'
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  message = FALSE,
  warging = FALSE
  )
```



<!-- 
          Projet de constituer un Tableau de bord traitant des VP, VUL et CATL  
          Structure du document objectivé par le groupe 
          cf. doc/specifications_editoriales.Rmd             
-->



```{r prologue-chargement librairies}
## chargement de l'univers tidyverse"
library('tidyverse')
library('ggrepel')
## chargement des données
source('configuration_locale.R')
tb_neuf <- rsvero2Light$neuf
```


## entête de d'identitification sur service émetteur

## titre + encart de présentation du document


!! NOUVEAUTE : encart avec texte conjoncturel à saisir !!

## titre 1 : répartition des immatriculations de voitures neuves en Juin 2021, avec zoom sur les motorisations alternatives

  - fig.1 : camembert double, parts en pourcentage des différentes 
    catégories de véhicule en termes de motorisation avec       
    zoom particulier sur les catégories dites alternatives
  
    inspiration possible ![exemple statinfo véhicules](../doc/illustrations/ce_20220429_0934.png)
    
    
```{r}
table_correspondance <- tibble(
  ener_code = c("ELH", "ESS", "DIE", NA, "DNR", "ENR", "HRE", "GAZ", "AUT"), 
  energie = c("Electrique et hydrogène", "Essence thermique", "Diesel thermique", 
              NA, "Diesel - hybride NR", "Essence - hybride NR", 
              "Hybride rechargeable", "Gaz", "Autres"),
       clsrgpmt = c(
         'c.ENR',
         'c.ESS',
         'c.DIE',
         NA,
         'c.DIE',
         'c.ESS',
         'c.ENR',
         'c.ENR',
         'autre'
       ))
```
    
    
```{r}
tablo_mn <- tb_neuf %>% 
  filter(
    REG == params$cogreg,
    annee == params$annee,
    mois == params$mois,
    categ == 'VP'
  ) %>% 
  inner_join(table_correspondance, by=c("ener_code"="ener_code")) %>% 
  select(REG,categ,nb_immat,ener_code,clsrgpmt) %>% 
  group_by(REG,categ,ener_code,clsrgpmt) %>% 
  summarize(eff_mn=sum(nb_immat))

tablo_mnm1 <- tb_neuf %>% 
  filter(
    REG == params$cogreg,
    annee == params$annee,
    mois == params$mois-1,
    categ == 'VP'
  ) %>% 
  inner_join(table_correspondance, by=c("ener_code"="ener_code")) %>% 
  select(REG,categ,nb_immat,ener_code,clsrgpmt) %>% 
  group_by(REG,categ,ener_code,clsrgpmt) %>% 
  summarize(eff_mnm1=sum(nb_immat))
```

```{r}
tablo_ventil <- tablo_mn %>% 
  left_join(tablo_mnm1) %>% 
  inner_join(table_correspondance)

tot_eff_mn   <- tablo_ventil %>% pull(eff_mn) %>% sum(na.rm = TRUE)
tot_eff_mnm1 <- tablo_ventil %>% pull(eff_mnm1) %>% sum(na.rm = TRUE)

tablo_ventil_cmplt <- tablo_ventil %>% 
  mutate(
    prct_eff_mn = (eff_mn / tot_eff_mn) * 100,
    prct_eff_mnm1 = (eff_mnm1 / tot_eff_mnm1) * 100,
    evo = round(prct_eff_mn - prct_eff_mnm1,1),
    etiquette = glue::glue("{energie} : \n{evo}pt")
  )
```


```{r}
d2plot <- tablo_ventil_cmplt %>% 
  ungroup() %>% 
  mutate(
    det = fct(ener_code, levels=c('ELH', 'HRE', 'DNR', 'DIE', 'ENR', 'ESS'))
  ) %>% 
  arrange(det) %>% 
  mutate(
    cls = fct_inorder(clsrgpmt),
  ) %>% 
  mutate(
    prct_cum = cumsum(prct_eff_mn),
    pos = 100 - ( prct_eff_mn/2 + lag(prct_cum) ),
    pos = if_else(is.na(pos), 100 - (prct_eff_mn/2), pos)
  ) %>% 
  select(cls,det,etiquette,prct_eff_mn, prct_cum, pos)  
  

```


```{r camember-double-anneaux}
namedColor <- c(
      'c.DIE'='#2E4053',
      'c.ESS'='#1976D2',
      'c.ENR'='#64DD17',
      'ENR'='#2196F3',
      'ESS'='#5499C7',
      'DNR'='#B0BEC5',
      'DIE'='#78909C',
      'ELH'='#76FF03',
      'HRE'='#7CB342'
    )

ggplot(d2plot) +
  geom_col(
    data = d2plot %>% group_by(cls) %>% summarise(eff=sum(prct_eff_mn)),
    mapping=aes(x=1, y=eff, color=cls, fill=cls, label=cls, width=0.6) 
  ) +
  geom_col(
    mapping=aes(x=1.6, y=prct_eff_mn, color=det, fill=det, label=det, width=0.8)
  ) +
  coord_polar(theta="y") +
  scale_fill_manual( values = namedColor ) +
  scale_color_manual( values = namedColor ) +
  theme_void() +
  xlim(0,5) +
  # ylim(0,120) +
  geom_label_repel(
    aes(
      x = 2,
      y = pos,
      label = etiquette,
    ),
    nudge_x = c(3,  3,    3,   3,   3,  3),
    nudge_y = c(2,  1,  -10, -10, -10,  0),
    show.legend = FALSE,
    fill = '#FEF9E7'
  )
ggsave('test.svg', width = 5, height = 5, device='svg', dpi=700)

```

    
    
## titre 1 : Immatriculations cumulées sur douze mois des voitures neuves depuis 2016

  - fig.2 : "

  - fig.3 : courbe double selon l'utilisateur (personne morale/personne physique)
  
  - fig.4 : chroniques selon le type de motorisation
  
  - fig.5 : chroniques selon le type de motorisation selon l'utilisateur

  - fig.6 : zoom sur les voitures neuves à motorisation alternative

---  ## titre 1 : Emissions moyens de CO2 des voitures neuves depuis 2016
---  
---    - fig.7 : chronique des émissions moyennes (courbe présentant la fameuse rupture 
---      méthodologique)
---  

## titre 1 : Immatriculations dans les DOM (hors Mayotte) en Juin 2021

  - fig.8 : histogramme par département selon les types de motorisations (note : bleu hybride)

  - fig.9 : tableau effectifs autres types de véhicules neufs par département et par genre
  
## glossaire / liste de définitions

## encart de liens

  - page de référence de la Deal
  - lien avec la publication nationale
  
## encart "crédits  
