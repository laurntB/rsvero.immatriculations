# Publication mensuelle sur les immatriculations de véhicules neufs
__Philippe Mouty__ (Responsable de l'_Unité_ Données Statistiques, _Pôle_ Connaissances du Territoire et Prospectives, _Service_ Prospective, Aménagement et Connaissance du Territoire, de la _DEAL_ Guadeloupe) et son équipe se sont lancés dans ce premier projet R collaboratif : la publication mensuelle sur les immatriculations de véhicules neufs.

Les données sont issues du Répertoire Statistique des Véhicules Routiers, 'RSVéRo', et offrent notamment des informations sur le nombre d’immatriculations neuves des voitures particulières du mois écoulé, par région, par département, par statut de l'acheteur, par type de motorisation, par énergie et par émissions de CO2.

## Projet inspiré de la publication mensuelle de la DREAL __Hauts-de-France__
__Didier Paluch__ (Chef du _Pôle_ Atelier des Données, _Service_ Information, Développement Durable et Evaluation Environnementale, de la _DREAL_ Hauts-de-France) et son
équipe (Jacques Laude et Gaëtan Boulet notamment) ont développé une première version de publication automatisée sur les immatriculations de véhicules neufs.  
L'intégralité du projet R nous a été transmise dans un but d'adaptation à la _DEAL_ Guadeloupe. Merci à eux !
